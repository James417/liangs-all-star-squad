/*
 * James Julian Currie
 */
package manageusers;

/**
 *
 * @author Jamie C
 */
public class Cashier extends Employee {

    public Cashier(String username, String password, int accountNum) {
        super(username, password, accountNum);
    }
    //again more will be added if this class is used for all prototypes
    //based on the class diagram. Right now this is for the prototype
}
