/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageusers;

/**
 *
 * @author Jamie C
 */
abstract class Employee {
    protected String username;
    protected String password;
    protected int accountNum;
    //might add more here later in terms of login and system boot/shutdown
    //if this class gets used by everyone besides me

    public Employee(String username, String password, int accountNum) {
        this.username = username;
        this.password = password;
        this.accountNum=accountNum;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
