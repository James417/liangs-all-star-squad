/*
 * James Julian Currie
 */
package manageusers;

/**
 *
 * @author Jamie C
 */
public class Manager extends Employee {

    public Manager(String username, String password, int accountNum) {
        super(username, password, accountNum);
    }
    //once again more can be added here if it ends up being used by everyone
    
}
